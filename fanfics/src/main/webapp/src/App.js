import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic';
import { I18nProvider, LOCALES } from './components/i18n';
import LoginContainer from './components/login/LoginContainer';
import UsersContainer from './components/users/UsersContainer';
import ArticlesContainer from './components/articles/ArticlesContainer';
import ThemeLangContainer from './components/themeAndLang/ThemeAndLangContainer';
import CredentialsContainer from './components/credentials/CredentialsContainer';
import ArticleOverviewContainer from './components/articleOverview/ArticleOverviewContainer';
import EditChapterContainer from './components/editChapter/EditChapterContainer';
import EditArticleContainer from './components/editArticle/EditArticleContainer';
import ConfirmRegistrationContainer from './components/confirm/ConfirmRegistrationContainer';
import './App.css';
import logo from './logo.svg';

const App = (props) => {
    return (
        <I18nProvider locale = {props.language === 'RU'? LOCALES.RUSSIAN : LOCALES.ENGLISH}>
            <AlertProvider template={AlertTemplate}>
                <div className = {props.theme === 'DARK' ? "dark" : "light"}>
                   <CredentialsContainer />
                       <header className="App-header">
                           <img src={logo} className="App-logo" alt="logo" />
                           <Switch>
                               <Route exact path='/login' render = { () => <LoginContainer />}/>
                               <Route exact path='/create' render = { () => <LoginContainer />}/>
                               <Route path='/confirm' render = { () => <ConfirmRegistrationContainer />}/>
                               <Route exact path='/' render = { () => <ArticlesContainer />}/>
                               <Route exact path='/myArticles/:myArticles' render = { () => <ArticlesContainer />}/>
                               <Route exact path='/articles/edit' render = { () => <EditArticleContainer />}/>
                               <Route path='/articles/edit/:id' render = { () => <EditArticleContainer />}/>
                               <Route path='/articles/:id' render = { () => <ArticleOverviewContainer />}/>
                               <Route exact path='/users' render = { () => <UsersContainer />}/>
                               <Route path='/chapters/:id?' render = { () => <EditChapterContainer />}/>
                               <Route path='/chapters' render = {() => <EditChapterContainer />}/>
                           </Switch>
                       </header>
                   <ThemeLangContainer/>
                </div>
            </AlertProvider>
        </I18nProvider>
    )
}

let mapStateToProps = (state) => {
  return {      
      theme: state.themeBar.theme,
      language: state.themeBar.language
  }
}

export default connect(mapStateToProps, {
  })(App);