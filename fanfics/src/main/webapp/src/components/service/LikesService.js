import { Component } from 'react';
import BaseService from './BaseService';

const LIKES_API_URL = 'https://fanfics-app.herokuapp.com/api/likes'

class CommentsService extends Component {
    constructor(props) {
        super(props);
        this.postLike = this.postLike.bind(this);
    }

    postLike(props) {
        return BaseService.post({url : LIKES_API_URL,
                                 data : props.data,
                                 messageId : 'create-like-error',
                                 alert: props.alert,
                                 config : {jwt: props.user.jwt} })
        
    }
}

export default new CommentsService()