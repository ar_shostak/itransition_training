import React from 'react';
import { withRouter } from 'react-router-dom';
import translate from './../../i18n/translate';

let Actions = (props) => {

    let createArticle = () => {
        props.history.push('/articles/edit');
    }

    let editArticle = () => {
        props.history.push(`/articles/edit/${props.checked.id}`);
    }

    return (
        <div>
            <button type = 'button' disabled = {props.checked.id === undefined? false : true} onClick = {createArticle} className = 'button'>{translate('create-button')}</button>
            <button type = 'button' disabled = {props.checked.id === undefined? true : false} onClick = {props.deleteArticle} className = 'button'>{translate('delete-button')}</button>
            <button type = 'button' disabled = {props.checked.id === undefined? true : false} onClick = {editArticle} className = 'button'>{translate('edit-button')}</button>
        </div>
    )
}

let WithRouteActions = withRouter(Actions);

export default WithRouteActions;