import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Content from './content/Content';
import ContentTable from './contentTable/ContentTable';
import translate from './../i18n/translate';

let ArticleOverview = (props) => {
  
    let countRate = () => {
        if(props.article.rates !== undefined && props.article.rates.length !== 0) {
            let values = props.article.rates.map(element => (element.value));
            return values.reduce((partial_sum, a) => partial_sum + a, 0) / values.length; 
        }
        return 0;
    }

    let isRateLeft = () => {
        if(props.article.rates !== undefined && props.article.rates.length !== 0) {
            let usersId = props.article.rates.map(element => (element.user.id));
            if(usersId.includes(props.user.id)){
                return true;
            }
        }
        return false; 
    }

    return (
    <Container fluid>
        <Row>
            <Col >
                <Row>
                    <Col md = 'auto'><h3>{props.article.name}</h3></Col>
                    <Col xs={4}><h5>{translate('rate')} : {countRate()}</h5></Col>
                    <Col ></Col>
                </Row>
                <Row >
                    <Col>
                        <Content chapters = {props.chapters}
                                 postComment = {props.postComment}
                                 user = {props.user}
                                 postLike = {props.postLike}
                                 postRate = {props.postRate}
                                 isRateLeft = {isRateLeft()}
                                 comments = {props.comments}/>
                    </Col>
                </Row>
            </Col>
            <Col xs = {1}/>
            <Col xs={4}><ContentTable  checked = {props.checked}
                                       isOwner = {props.isOwner}
                                       user = {props.user}
                                       doAction = {props.doAction}
                                       checkChapter = {props.checkChapter} 
                                       chapters = {props.chapters}/></Col>
        </Row>
</Container>
    )
}

export default ArticleOverview;