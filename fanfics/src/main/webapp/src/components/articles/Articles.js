import React from 'react';
import ActionBar from './articlesContent/../actionBar/ActionBar';
import ArticlesTable from './articlesContent/ArticlesTable';

let Articles = (props) => {
    return (
        <div>
            <ActionBar deleteArticle = {props.deleteArticle}
                       isOnMyPage = {props.isOnMyPage}
                       searchText = {props.searchText}
                       checked = {props.checked}
                       user = {props.user}/>
            <ArticlesTable articles = {props.articles}
                           checked = {props.checked} 
                           checkArticle = {props.checkArticle}/>
        </div>
    )
}

export default Articles;