import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextArea, MySelect, RenderSelectInput } from './../utils/CustomTags';
import translate from '../i18n/translate';
import { required } from './../validator/CrUpdValidator';
import './EditArticle.css';

let EditArticle = (props) => {

    let handleSubmit = (formData) => {
       
        let getTags = () => {
            if(formData.tags){
                return formData.tags.map(element => element.value);
            } 
        }

        let article = {
            name : formData.name,
            genre : formData.genre,
            description : formData.description === undefined ? null : formData.description ,
            tags : getTags(), 
            id : props.article.id === undefined ? null : props.article.id ,
            };     
        props.produceArticle(article); 
    }

    let getPlaceholders = () => {
        if (props.article.id === undefined) {
            return {name : 'name',
                    genre : 'genre',
                    description : 'description',
                    tags : 'tags-column'};
        } else {
            return props.article;
        }
    }                    

    return(
        <div>
          <div>{translate('fill-in-headline')}</div>  
          <EditArticleReduxForm onSubmit = {handleSubmit}
                                article = {props.article}
                                genres = {props.enumsMap.genres}
                                tags = {props.enumsMap.tags}
                                placeholders = {getPlaceholders()}/>
        </div>
    )
}

const EditArticleForm = (props) => {
    return(
        <div className = 'formContainer'>
            <form onSubmit={props.handleSubmit}> 
                <Field placeholder = {`${props.placeholders.name}`} name = 'name' validate = {required}
                            component = {TextArea} className='ticketInput'/>  
                <Field placeholder = {`${props.placeholders.genre}`} name = 'genre' validate = {[required]}
                            component = {MySelect} enum = {props.genres} />
                <Field placeholder = {`${props.placeholders.description}`} name = 'description'
                            component = {TextArea} className='ticketInput'/>
                <Field  placeholder = {`${props.placeholders.tags.length === 0? 'tags-column' : props.placeholders.tags}`} name = {'tags'}
                                                        object = {props.tags} component = {RenderSelectInput}/>
                <div><button type = 'submit' className = 'button'>{translate('save')}</button></div>
            </form>
        </div>
    )
}

const EditArticleReduxForm = reduxForm( { form : 'login' } ) (EditArticleForm)
export default EditArticle;