package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException {
        validateArgs(args);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                byte[] key = Utils.generateKey();
                int computersAction = Utils.getRandomIndex(args.length);
                String computersActionName = args[computersAction];
                Printer.showHeader(Utils.calcHMAC(key, computersActionName));
                Printer.showMenu(args);
                Integer userAction;
                while (true) {
                    userAction = readUsersAction(reader);
                    if (!validateUserAction(args.length, userAction)) {
                        Printer.showException("Wrong argument. Try again");
                        Printer.showMenu(args);
                        continue;
                    }
                    break;
                }
                if (userAction == -1) {
                    break;
                }
                Boolean isUserWin = isUserWon(userAction, computersAction, args.length);
                Printer.showResult(args[userAction], computersActionName, isUserWin,
                        Base64.getEncoder().encodeToString(key));
            }
        }
    }

    private static void validateArgs(String[] args) {
        if (args.length < 3 || args.length % 2 == 0) {
            throw new IllegalArgumentException("Wrong number of arguments");
        } if (args.length != Arrays.stream(args).collect(Collectors.toSet()).size()) {
            throw new IllegalArgumentException("Duplicate arguments detected");
        }
    }

    private static boolean validateUserAction(int length, Integer actionCode) {
        return actionCode != null && length > actionCode;
    }

    private static Integer readUsersAction(BufferedReader reader) {
        try {
            return Integer.parseInt(reader.readLine()) - 1;
        } catch (NumberFormatException e) {
        } catch (IOException e) {
            Printer.showException("Exception while reading action. Try again");
        }
        return null;
    }

    private static Boolean isUserWon(int userIndex, int compIndex, int length) {
        if (userIndex == compIndex) {
            return null;
        }
        int half = (length - 1) / 2;
        List<Integer> winners = new ArrayList<>();
        for (int i = userIndex + 1; i <= half + userIndex; i++) {
           winners.add(i > (length - 1) ? i - length :  i);
        }
        return !winners.contains(compIndex);
    }


}
