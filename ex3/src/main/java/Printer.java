package com.company;

public class Printer {
    private static final String HMAC_DECLARATION = "HMAC: ";

    public static void showHeader(String hmac) {
        System.out.println("New Game\n" + HMAC_DECLARATION + hmac);
    }

    public static void showMenu(String[] actions) {
        StringBuilder builder = new StringBuilder();
        builder.append("Available movies:\n");
        int i = 1;
        for(String act : actions) {
            builder.append(i).append(" - ").append(act).append("\n");
            i++;
        }
        builder.append("0 - exit\n");
        builder.append("Enter your move: ");
        System.out.println(builder);
    }

    public static void showResult(String userActionName, String computerActionName, Boolean isUserWon, String key) {
        StringBuilder builder = new StringBuilder();
        builder.append("Your move: ").append(userActionName).append("\n");
        builder.append("Computer move: ").append(computerActionName).append("\n");
        if (isUserWon == null) {
            builder.append("Draw!\n");
        } else {
            builder.append(isUserWon ? "You win!" : "Computer win!").append("\n");
        }
        builder.append("HMAC key: ").append(key).append("\n");
        System.out.println(builder);
    }

    public static void showException(String exceptionText) {
        System.out.println(exceptionText);
    }
}
