import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import './../ArticleOverview.css';
import likeicon from './../../../img/like.png';

let Chapters = (props) => {
    let chapters = props.chapters;
    let addLike = (id) => {
        props.postLike(id);
    }

    let displayAttachment = (attachment) => {
        if (attachment !== undefined && attachment !== null){
            return (
                <img src={`data:${attachment.contentType};base64,${attachment.blobBytes}`} alt="ticketinfo.attachment" width={150} />
            )  
        } 
    }
    return (
        <Container>
            {chapters.map(
                (chapter) => 
                    <Container fluid className = 'padding-wrapper'>
                        <Row >
                            <Col md = 'auto'><h5>{chapter.number}. {chapter.name}</h5></Col>
                            <Col md = 'auto'><button disabled = {props.user.id === '' ? true : false } onClick = {() => addLike(chapter.id)} className = 'button'><img src = {likeicon} width="20" height="20" alt = ''/></button></Col>
                            <Col md = 'auto'>{chapter.likes}</Col>
                            <Col/>
                        </Row>
                        <Row>{displayAttachment(chapter.attachment)}</Row>
                        <Row>{chapter.content === 'null' ? '' : chapter.content }</Row>
                    </Container>
            )} 
        </Container>
    ) 
}

export default Chapters;