import React from 'react';
import translate from '../../i18n/translate';

const Header = (props) => {

    let blockUsers = () => {
       props.updateUsers({action : 'BLOCK'});
    }

    let getAdminRules = () => {
        props.updateUsers({action : 'ADMIN'});
    }

    let unblockUsers = () => {
        props.updateUsers({action : 'UNBLOCK'});
    }

    return (    
        <div>
             <nav className='switchBar'>
                 <div>
    <button onClick = {blockUsers} id = 'Button1' className ='button'>{translate('block-button')}</button>
                      <button onClick = {unblockUsers} id = 'Button2' className ='button'>{translate('unblock-button')}</button>
                      <button onClick = {getAdminRules} id = 'Button4' className ='button'>{translate('get-admins-rules-button')}</button>
                 </div>
             </nav>
          </div>
    )
}

export default Header;