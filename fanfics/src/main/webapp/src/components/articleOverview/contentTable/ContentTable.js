import React from 'react';
import { withRouter } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import { Checkbox } from '../../utils/CustomTags';
import translate from '../../i18n/translate';

let ContentTable = (props) => {

    let handleChange = (e) => {
         props.checkChapter({checked : e.currentTarget.name});
    } 

    let handleSubmit = (action) => {
        props.doAction(action);
    }

    let isChaptersEmpty = () => {
        return ( props.chapters.length === 0 ? true : false )
    }

    let showButtons = () => {
        if(props.isOwner === true) {
            return (
                <Col>
                    <Row>
                        <Col><button type = 'button'
                                     onClick = {() => handleSubmit('CREATE')}
                                     className = {'button'}>{translate('create-button')}</button></Col>
                        <Col><button type = 'button'
                                     onClick = {() => handleSubmit('EDIT')}
                                     disabled = {isChaptersEmpty()}
                                     className = {'button'}>{translate('edit-button')}</button></Col>
                    </Row>
                    <Row>                 
                        <Col><button type = 'button'
                                     onClick = {() => handleSubmit('DELETE')}
                                     disabled = {isChaptersEmpty()}
                                     className = {'button'}>{translate('delete-button')}</button></Col>
                    </Row>
                </Col>
            )
        }
    }

    return (
        <Container>
            <Row ><h4>{translate('content-table')}</h4></Row>
            {props.chapters.map(
                (chapter) => 
                    <Container fluid>
                        <Row >
                            <Col>
                                <h6>{chapter.number}. {chapter.name}</h6>
                            </Col>    
                            <Col md = 'auto'> 
                                <Checkbox name={chapter.id} disabled = {props.isOwner === true? false : true}
                                            checked={chapter.id === props.checked? true : false} onChange={handleChange} />
                            </Col> 
                        </Row>                    
                    </Container>
            )} 
            {showButtons()}
        </Container>
    )
}

export default withRouter(ContentTable);