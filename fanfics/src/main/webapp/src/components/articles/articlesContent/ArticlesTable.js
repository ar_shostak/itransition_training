import React from 'react';
import { Checkbox } from '../../utils/CustomTags';
import { NavLink } from 'react-router-dom';
import translate from './../../i18n/translate';

let ArticlesTable = (props) => {

    let handleChange =(article) => {
        props.checkArticle(article);
     }

     let getTags =(article) => {
         if(article.tags !== undefined && article.tags.length !== 0) {
             return article.tags.map(value => '#' + value).toString();
         }
     }

     let countRate = (article) => {
        if(article.rates !== undefined && article.rates.length !== 0) {
            let values = article.rates.map(element => (element.value));
            return values.reduce((partial_sum, a) => partial_sum + a, 0) / values.length; 
        }
        return '-';
    }
 
     let header = () => {
         return (
             <thead>
                 <tr>
                     <th className='col-1'>{translate('select-article-column')}</th>
                     <th className='col-3'>{translate('article-column')}</th>
                     <th className='col-2'>{translate('genre-column')}</th>
                     <th className='col-3'>{translate('rate')}</th>
                     <th className='col-3'>{translate('date-column')}</th>
                     <th className='col-3'>{translate('tags-column')}</th>
                 </tr>
             </thead>
         )
     };
 
     let body = (props) => {
         return (
             props.articles.map(
                 article =>
                     <tbody>
                         <tr key={article.id}>
                             <td><Checkbox name={article}
                                           checked={article.id === props.checked.id? true : false} onChange={()=>{handleChange(article)}} /></td>
                             <td><NavLink to={'/articles/' + article.id}>{article.name}</NavLink></td>
                             <td>{article.genre}</td>
                             <td>{countRate(article)}</td>
                             <td>{article.lastModifyDate}</td>
                             <td>{getTags(article)}</td>
                         </tr>
                     </tbody>
             )
         )
     }

     return (
          <table className="table">
             {header()}
             {body(props)}
         </table>
     )
 }

export default ArticlesTable;