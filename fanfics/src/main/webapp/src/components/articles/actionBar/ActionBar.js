import React from 'react';
import Actions from './Actions';
import './ActionBar.css';
import Search from './search/Search';
import { Container, Row, Col } from 'react-bootstrap';

let ActionBar = (props) => {
    return (
        <div className={props.user.id === ''  ? 'hidden' : ''}>
        <Container>
            <Row className="justify-content-md-center">
                <Search searchText = {props.searchText} user = {props.user}/>
                <Col md='auto'/>
    <Col md='auto'>{props.isOnMyPage === true? <Actions  checked = {props.checked} deleteArticle = {props.deleteArticle}/> :<div/>}</Col>
            </Row>
        </Container>
        </div>
    )
}

export default ActionBar;