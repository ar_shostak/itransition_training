import React from 'react';
import { Navbar, Nav, Form } from 'react-bootstrap';
import { withRouter, NavLink } from 'react-router-dom';
import translate from './../i18n/translate';
import './Credentials.css';
import homeicon from './../../img/homeicon.png';
import mypageicon from './../../img/mypageicon.png';
import './../users/header/ActionBar.css';

const Credentials = (props) => {
    let user = props.user;

    let isUserRegistered = () => {
        if (user.id !== '') {
            return true;
        } return false;
    }

    let isAdmin = () => {
        if (isUserRegistered && user.role === 'ADMIN') {
             return true;
        } return false;
    }

    let credentData = () => {
        if(isUserRegistered() && user.firstName !== "") {
            return (<div className = 'credLight'>{`${user.lastName} ${user.firstName}`}</div>) 
        } else {
            return (<div>{translate('unautorized')}</div>)
        }
    }

    let handleUsersPage = () => {
        if(isUserRegistered && user.role === 'ADMIN') {
            props.history.push('/users');
        }
    }

    let logout = () => {
        return (
            <NavLink to={'/'}>
                <button onClick = {props.logout} className = 'button'>{translate('logout-button')}</button>
            </NavLink>
        )
    }

    let loginOrRegister = () => {
        return (
           <NavLink to={'/login'}>
               <button className = 'button'>{translate('login-or-register-button')}</button>
           </NavLink>
        )
    }

    return(
        <Navbar >
            <Nav><NavLink to={'/myArticles/myArticles'}><button hidden = {isUserRegistered() === true ? false : true} className = 'button'><img src = {mypageicon} width = '50' alt = ''/></button></NavLink></Nav>
            <Nav><NavLink to={'/'}><button className = 'button'><img src = {homeicon} alt = ''/></button></NavLink></Nav>
            <Nav className='mr-auto'>
                <button onClick = {handleUsersPage} disabled = {isAdmin() === true ? false : true} className = 'button'>{translate('users-managment-button')}</button>
            </Nav>
            <Form>
                <Navbar.Text>{credentData()}</Navbar.Text>
                <Nav>{isUserRegistered() === true ? logout() : loginOrRegister()}</Nav>
            </Form>
           
        </Navbar>
    )
}

let WithRouteCredentials = withRouter(Credentials);

export default WithRouteCredentials;