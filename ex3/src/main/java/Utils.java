package com.company;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;
import java.util.Random;

public class Utils {
    private static final String ALGORITHM_NAME = "HmacSHA256";

    public static int getRandomIndex(int length) {
        Random rand = new Random();
        return rand.nextInt(length);
    }

    public static String calcHMAC(byte[] secretKey, String action) {
        byte[] hmac;
        try {
            Mac mac = Mac.getInstance(ALGORITHM_NAME);
            SecretKeySpec secretKeySpec = new SecretKeySpec(secretKey, ALGORITHM_NAME);
            mac.init(secretKeySpec);
            hmac =  mac.doFinal(action.getBytes());
        } catch (InvalidKeyException | NoSuchAlgorithmException exc) {
            throw new RuntimeException("Failed to calculate hmac-sha256", exc);
        }
        return Base64.getEncoder().encodeToString(hmac);
    }

    public static byte[] generateKey() {
        byte[] bytes = new byte[32];
        new SecureRandom().nextBytes(bytes);
        return bytes;
    }
}
